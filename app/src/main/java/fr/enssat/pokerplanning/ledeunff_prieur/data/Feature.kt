package fr.enssat.pokerplanning.ledeunff_prieur.data

import com.google.gson.Gson
import fr.enssat.pokerplanning.ledeunff_prieur.functions.generateUID

data class Feature(
    override val id: String = generateUID(),
    val session_id: String = "",

    var title: String = "",
    var description: String = "",

    val votes: MutableList<Vote> = mutableListOf()
):DataClass{
    companion object{
        fun fromJson(json: Any?): Feature{
            return Gson().fromJson(json.toString(), Feature::class.java)
        }
    }

    override fun toString(): String{
        return Gson().toJson(this)
    }

}
