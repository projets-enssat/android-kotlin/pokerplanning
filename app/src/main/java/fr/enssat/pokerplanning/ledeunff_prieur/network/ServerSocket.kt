package fr.enssat.pokerplanning.ledeunff_prieur.network

import android.os.Handler
import android.os.Looper
import android.util.Log
import fr.enssat.pokerplanning.ledeunff_prieur.constant.MESSAGE_MAX_SIZE
import fr.enssat.pokerplanning.ledeunff_prieur.data.Message
import java.io.IOException
import java.net.InetAddress
import java.net.ServerSocket
import java.net.Socket
import java.net.SocketException
import java.nio.charset.StandardCharsets
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class ServerSocket(private val response: (Message) -> Message?) {
    private val TAG = this.javaClass.simpleName

    companion object {
        const val PORT = 6791
    }

    private lateinit var serverSocket: ServerSocket
    private val MAX_CLIENT_BACK_LOG = 50
    private var loop = true

    //liste des clients connectés au serveur
    private val allClientsConnected = mutableListOf<Reply>()

    fun startListening(ip_address: InetAddress) {
        serverSocket = ServerSocket(PORT, MAX_CLIENT_BACK_LOG, ip_address)

        // écoute toutes les nouvelles demandes de connections clientes
        // et crée une socket locale au serveur, reply dédiée a ce nouveau client.
        Executors.newSingleThreadExecutor().execute {
            while (loop) {
                try{
                    val newSocket = serverSocket.accept()
                    allClientsConnected.add(Reply(newSocket, response))
                }catch (e: SocketException){
                    Log.d("network", "tcp socket server closed")
                    loop = false
                }
            }
        }
    }

    fun stopListening() {
        loop = false
        allClientsConnected.forEach { it.stop() }
        try{
            serverSocket.close()
        }catch(e: UninitializedPropertyAccessException){

        }
    }

    class Reply(val socket: Socket, val response: (Message) -> Message?) {
        val TAG = this.javaClass.simpleName
        var loop = true
        val address = socket.inetAddress.address.toString()

        private val mainThread = object : Executor {
            val handler = Handler(Looper.getMainLooper())
            override fun execute(command: Runnable) {
                handler.post(command)
            }
        }

        init {
            Executors.newSingleThreadExecutor().execute {
                try {
                    var message: Message? = null

                    while (loop) {
                        try {
                            val buffer = ByteArray(MESSAGE_MAX_SIZE)
                            val len = socket.getInputStream().read(buffer)
                            val json = String(buffer, 0, len, StandardCharsets.UTF_8)
                            message = Message.fromJson(json)
                        }catch (e: StringIndexOutOfBoundsException){
                            // le message a une taille nulle
                            message = null
                        }

                        // determine la reponse
                        val resp = if(response != null && message!=null) response(message) else null

                        // si une reponse est prévue la renvoie
                        if(resp != null){
                            val data = resp.toString().toByteArray()
                            socket.getOutputStream().write(data)
                            socket.getOutputStream().flush()
                        }
                    }
                } catch (e: IOException) {
                    Log.d(TAG, "Client $address down")
                }
            }
        }

        fun stop() {
            loop = false
            socket.close()
        }
    }
}
