package fr.enssat.pokerplanning.ledeunff_prieur.network


import android.os.Handler
import android.os.Looper
import android.os.StrictMode
import android.util.Log
import fr.enssat.pokerplanning.ledeunff_prieur.constant.MESSAGE_MAX_SIZE
import java.net.*
import java.nio.charset.Charset
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class BroadcastAgent(val listener: (String) -> Unit) {

    companion object {
        val PORT = 6789
    }


    private val TAG = this.javaClass.simpleName
    private val socket = createSocket()
    private var loop = true

    private val mainThread = object : Executor {
        val handler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            handler.post(command)
        }
    }


    fun createSocket(): DatagramSocket {
        val socket = DatagramSocket(null)
        val sockaddr = InetSocketAddress(InetAddress.getByName("0.0.0.0"), PORT)
        socket.broadcast = true
        socket.reuseAddress = true
        socket.bind(sockaddr)
        return socket
    }


    fun startReceiveLoop() {
        Executors.newSingleThreadExecutor().execute {
            while (loop) {
                try{
                    val data = receive()
                    mainThread.execute { listener(data) }
                }catch(e: SocketException){
                    loop = false
                }
            }
        }
    }

    private fun receive(): String {
        var buffer = ByteArray(MESSAGE_MAX_SIZE)
        val packet = DatagramPacket(buffer, MESSAGE_MAX_SIZE)

        Log.d(TAG, "waiting for multicast datagram")
        socket.receive(packet)

        val data = String(packet.data, packet.offset, packet.length, Charset.forName("UTF-8"))
        Log.d(TAG, "received from " + socket.remoteSocketAddress + ", " + data)

        return data
    }


    fun stopReceiveLoop() {
        loop = false
        _closeSocket()
    }

    private fun _closeSocket() {
        Log.d(TAG, "closing multicast socket")
        socket.close()
    }

    fun send(msg: String, stopAfter: Boolean = false) {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        Executors.newSingleThreadExecutor().execute {
            val data = msg.toByteArray(Charset.forName("UTF-8"))
            Log.d(TAG, "publishing on multicast: ${String(data)}")
            val packet =
                DatagramPacket(data, 0, data.size, InetAddress.getByName("255.255.255.255"), PORT)
            socket.send(packet)

            if(stopAfter){
                stopReceiveLoop()
            }
        }
    }

}