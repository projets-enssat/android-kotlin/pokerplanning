package fr.enssat.pokerplanning.ledeunff_prieur.ui.createsession

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.constant.HOSTFRAGMENT
import fr.enssat.pokerplanning.ledeunff_prieur.data.Session
import fr.enssat.pokerplanning.ledeunff_prieur.databinding.FragmentCreateSessionBinding
import fr.enssat.pokerplanning.ledeunff_prieur.functions.getViewModel


class CreateSessionFragment : Fragment() {
    private lateinit var binding: FragmentCreateSessionBinding
    private lateinit var sessionViewModel: SessionViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        sessionViewModel = getViewModel()

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_session, container, false)
        binding.vm  = sessionViewModel

        sessionViewModel.onCreateSessionValidationError = {
            Toast.makeText(
                this.context,
                "Missing session title.",
                Toast.LENGTH_SHORT
            ).show()
        }

        activity?.let {
            sessionViewModel.onCreateSession = { sessionViewModel.createSession(it) }
        }

        return binding.root
    }
}
