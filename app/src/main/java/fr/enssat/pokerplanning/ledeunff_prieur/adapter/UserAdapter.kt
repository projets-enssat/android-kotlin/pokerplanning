package fr.enssat.pokerplanning.ledeunff_prieur.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.data.User
import fr.enssat.pokerplanning.ledeunff_prieur.data.Vote


class UserViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    private val itemName = view.findViewById<TextView>(R.id.itemName)
    private val voteCount = view.findViewById<TextView>(R.id.itemVote)

    fun setUser(user: User, position: Int, vote: Vote? = null) {
        itemName.text = "${(position + 1)}.\t${user.last_name} ${user.first_name}"
        if (vote != null) voteCount.text = vote.value.toString()
    }
}

class UserAdapter(val listener: (String) -> Unit): RecyclerView.Adapter<UserViewHolder>() {

    var users: List<User> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    var votes: List<Vote> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    private fun getVote(user: User): Vote? {
        for (vote in votes) {
            if( vote.user_id == user.id)
                return vote
        }
        return null
    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.recyclerview_item_user, parent, false)
        return UserViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.setUser(users[position], position=position, vote = getVote(users[position]))
    }

}