package fr.enssat.pokerplanning.ledeunff_prieur

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomnavigation.BottomNavigationView
import fr.enssat.pokerplanning.ledeunff_prieur.constant.CLIENTFRAGMENT
import fr.enssat.pokerplanning.ledeunff_prieur.functions.setMenuListener
import fr.enssat.pokerplanning.ledeunff_prieur.ui.joinsession.*

class JoinSessionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join_session)

        // retrieve session view model to provide router
        val sessionViewModel: JoinSessionViewModel = ViewModelProviders.of(this).get(JoinSessionViewModel::class.java)

        // declare router that contains a methode navigate to switch between fragments
        val router = Router(this)
        router.route(CLIENTFRAGMENT.LIST_SESSIONS, JoinSessionFragment::class.java)
        router.route(CLIENTFRAGMENT.WAITING_SESSION_START, WaitSessionStartFragment::class.java)
        router.route(CLIENTFRAGMENT.FEATURE_VOTE, FeatureVoteFragment::class.java)
        router.route(CLIENTFRAGMENT.WAITING_VOTES, WaitOtherParticipantsFragment::class.java)
        router.route(CLIENTFRAGMENT.SESSION_RESULTS, SessionResultsFragment::class.java)
        sessionViewModel.router = router

        // create and bind bottom nav menu
        val navigation: BottomNavigationView = findViewById(R.id.nav_view)
        setMenuListener(navigation, R.id.navigation_join_session)
    }

}