package fr.enssat.pokerplanning.ledeunff_prieur.ui.joinsession

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.adapter.ExpandableAdapter
import fr.enssat.pokerplanning.ledeunff_prieur.databinding.FragmentPublishResultsBinding
import fr.enssat.pokerplanning.ledeunff_prieur.databinding.FragmentSessionResultsBinding
import fr.enssat.pokerplanning.ledeunff_prieur.functions.getViewModel

class SessionResultsFragment : Fragment() {
    private lateinit var binding: FragmentPublishResultsBinding
    private lateinit var joinSessionViewModel: JoinSessionViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        joinSessionViewModel = getViewModel()

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_publish_results, container, false)

        val adapter = ExpandableAdapter {}
        binding.expandableRecycler.adapter = adapter

        joinSessionViewModel.currentSession.observe(this, Observer {session ->
            adapter.features = session.features
            adapter.users = session.users.toList()
        })

        return binding.root
    }
}