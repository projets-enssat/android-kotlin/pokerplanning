package fr.enssat.pokerplanning.ledeunff_prieur.ui.createsession

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.adapter.UserAdapter
import fr.enssat.pokerplanning.ledeunff_prieur.databinding.FragmentPublishVotesBinding
import fr.enssat.pokerplanning.ledeunff_prieur.functions.getViewModel

class PublishVotesFragment : Fragment() {
    private lateinit var binding: FragmentPublishVotesBinding
    private lateinit var sessionViewModel: SessionViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        sessionViewModel = getViewModel()

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_publish_votes, container, false)
        binding.vm  = sessionViewModel

        val adapter = UserAdapter {}
        binding.usersList.adapter = adapter

        sessionViewModel.users.observe(this, Observer {
            list -> adapter.users = list
        })
        sessionViewModel.votes.observe(this, Observer {
            list -> adapter.votes = list
        })

        return binding.root
    }

}