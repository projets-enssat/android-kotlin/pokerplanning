package fr.enssat.pokerplanning.ledeunff_prieur.ui.joinsession

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import fr.enssat.pokerplanning.ledeunff_prieur.CommonViewModel
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.Router
import fr.enssat.pokerplanning.ledeunff_prieur.constant.CLIENTFRAGMENT
import fr.enssat.pokerplanning.ledeunff_prieur.constant.HOSTFRAGMENT
import fr.enssat.pokerplanning.ledeunff_prieur.constant.MESSAGE
import fr.enssat.pokerplanning.ledeunff_prieur.data.*
import fr.enssat.pokerplanning.ledeunff_prieur.functions.addOrReplace
import fr.enssat.pokerplanning.ledeunff_prieur.network.BroadcastAgent
import fr.enssat.pokerplanning.ledeunff_prieur.network.ClientSocket
import fr.enssat.pokerplanning.ledeunff_prieur.ui.preferences.StoredPreferences

class JoinSessionViewModel : CommonViewModel() {
    lateinit var router: Router

    private val sessionsSet = mutableSetOf<Session>()
    private val mutableSessions = MutableLiveData<List<Session>>()
    val sessions: LiveData<List<Session>> get() = mutableSessions

    // on view change
    var onSessionClosed: ()->Unit = {}

    val onVoteFeature: (Int)->Unit = { value-> voteForFeature(value) }

    // data
    lateinit var currentUser: User

    var _currentSession = MutableLiveData<Session>()
    val currentSession: LiveData<Session> get() = _currentSession

    var _allUsers = MutableLiveData<List<User>>()
    val allUsers: LiveData<List<User>> get() = _allUsers

    var _currentFeature = MutableLiveData<Feature>()
    val currentFeature: LiveData<Feature> get() = _currentFeature

    lateinit var currentVote: Vote

    // network utils
    private val tcplient = ClientSocket(this::tcpMessageHandler, this::tcpStatusHandler)
    private val multicast = BroadcastAgent(this::multicastMessageHandler)

    fun addSession(session: Session){
        sessionsSet.addOrReplace(session)
        mutableSessions.postValue(sessionsSet.toList())
    }

    fun deleteSession(session_id: String){
        sessionsSet.removeIf{it.id == session_id}
        mutableSessions.postValue(sessionsSet.toList())
    }

    fun runMulticastAgent(context: Context){
        currentUser = StoredPreferences(context).currentUser()
        multicast.startReceiveLoop()
    }

    // tcp stuff
    fun tcpStatusHandler(status: Boolean){
        Log.d("poker", "TCP status changed: $status")
    }

    fun tcpMessageHandler(message: Message){
        Log.d("poker", "client received TCP message")
        Log.d("poker", "message: $message")
        if(message.type == MESSAGE.JOIN_SESSION_REPLY) {
            val session = Session.fromJson(message.data)
            _currentSession.postValue(session)

            // gere ce que fait le client quand il rejoint une session en cours
            if (session.currentFragment == HOSTFRAGMENT.WAIT_VOTES) {
                // le client rejoint une session pendant le vote, il redemande la feature
                tcplient.sendAndReceive(MESSAGE.FEATURE_VOTE_UPDATE)
            }
        }else if(message.type == MESSAGE.FEATURE_VOTE_REQUEST){
            _currentFeature.postValue(Feature.fromJson(message.data))
            router.navigate(CLIENTFRAGMENT.FEATURE_VOTE)
        }else if(message.type == MESSAGE.FEATURE_VOTE_REPLY){
            router.navigate(CLIENTFRAGMENT.WAITING_VOTES)
        }
    }

    fun joinSession(session: Session){
        tcplient.connect(session, currentUser)
    }

    // multicast stuff

    fun multicastEmit(msgType: MESSAGE = MESSAGE.NULL, session_id: String? = null, data: Any? = null){
        val message = Message(msgType, session_id, data=data.toString())
        multicast.send(message.toString())
    }

    fun multicastMessageHandler(json: String){
        val message = Gson().fromJson(json, Message::class.java)
        onHandleMulticastMessage(message)
    }

    fun onHandleMulticastMessage(message: Message){
        Log.d("poker", "client handled message "+message.type)
        if(message.type == MESSAGE.SESSIONS_REPLY && message.data != null) {
            Log.d("poker", "session_reply" + message.data)
            val session = Session.fromJson(message.data)
            addSession(session)
        }else if(currentSession.value != null && message.session_id == currentSession.value?.id){
            if(message.type == MESSAGE.FEATURE_VOTE_REQUEST){
                Log.d("poker", "new feature vote asked")
                _currentFeature.postValue(Feature.fromJson(message.data))
                router.navigate(CLIENTFRAGMENT.FEATURE_VOTE)
            }else if(message.type == MESSAGE.PUBLISH_VOTE_RESULTS){
                Log.d("poker", "new feature vote asked")
                val session = Session.fromJson(message.data)
                _allUsers.postValue(session.users)
                _currentFeature.postValue(session.features.last())
            }else if(message.type == MESSAGE.PUBLISH_SESSION_RESULTS){
                Log.d("poker", "publish session results received")
                val session = Session.fromJson(message.data)
                router.navigate(CLIENTFRAGMENT.SESSION_RESULTS)
                _currentSession.postValue(session)
            }else if(message.type == MESSAGE.SESSION_CLOSE) {
                onSessionClosed()
            }
        }else{
            Log.d("poker", "client BALLEC")
        }

        if(message.type == MESSAGE.SESSION_CLOSE && message.session_id != null) {
            Log.d("poker", "client close session ${message.session_id}")
            deleteSession(message.session_id)
        }
    }

    fun voteForFeature(value: Int){
        val session_id = currentSession.value?.id

        if(session_id != null){
            val vote = Vote(
                session_id = session_id,
                user_id = currentUser.id,
                value = value
            )
            tcplient.sendAndReceive(MESSAGE.FEATURE_VOTE_REPLY, session_id, data=vote)
        }
    }

    override fun leaveSession(){
        router.open(R.id.navigation_join_session)
    }

    override fun onCleared() {
        super.onCleared()
        multicast.stopReceiveLoop()
        //tcplient
        Log.d("poker", "joinsessionviewmodel cleared")
    }
}

