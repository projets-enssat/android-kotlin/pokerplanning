package fr.enssat.pokerplanning.ledeunff_prieur.ui.createsession

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.databinding.FragmentCreateFeatureBinding
import fr.enssat.pokerplanning.ledeunff_prieur.functions.getViewModel


class CreateFeatureFragment : Fragment() {
    private lateinit var binding: FragmentCreateFeatureBinding
    private lateinit var sessionViewModel: SessionViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        sessionViewModel = getViewModel()

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_feature, container, false)
        binding.vm  = sessionViewModel

        sessionViewModel.onCreateFeatureValidationError = {
            Toast.makeText(this.context, "Missing feature title.", Toast.LENGTH_SHORT).show()
        }

        return binding.root
    }

}
