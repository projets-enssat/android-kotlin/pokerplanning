package fr.enssat.pokerplanning.ledeunff_prieur.data

import com.google.gson.Gson
import fr.enssat.pokerplanning.ledeunff_prieur.constant.HOSTFRAGMENT
import fr.enssat.pokerplanning.ledeunff_prieur.functions.generateUID

data class Session(
    override val id: String = generateUID(),

    var title: String = "",
    var description: String = "",
    var ip_address: String = "",

    var users: List<User> = listOf(),
    var features: MutableList<Feature> = mutableListOf(),

    var currentFragment:HOSTFRAGMENT = HOSTFRAGMENT.CREATE_SESSION,

    var owner: User = User()

):DataClass{
    companion object{
        fun fromJson(json: Any?): Session{
            return Gson().fromJson(json.toString(), Session::class.java)
        }
    }

    override fun toString(): String{
        return Gson().toJson(this)
    }
}
