package fr.enssat.pokerplanning.ledeunff_prieur.ui.createsession

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.adapter.ExpandableAdapter
import fr.enssat.pokerplanning.ledeunff_prieur.databinding.FragmentPublishResultsBinding
import fr.enssat.pokerplanning.ledeunff_prieur.functions.getViewModel

class PublishResultsFragment : Fragment() {
    private lateinit var binding: FragmentPublishResultsBinding
    private lateinit var sessionViewModel: SessionViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        sessionViewModel = getViewModel()

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_publish_results, container, false)
        binding.vm  = sessionViewModel

        val adapter = ExpandableAdapter {}
        binding.expandableRecycler.adapter = adapter

        sessionViewModel.features.observe(this, Observer {
                list -> adapter.features = list
        })
        sessionViewModel.users.observe(this, Observer {
                list -> adapter.users = list
        })

        return binding.root
    }

}