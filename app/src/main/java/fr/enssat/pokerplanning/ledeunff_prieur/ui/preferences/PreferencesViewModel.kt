package fr.enssat.pokerplanning.ledeunff_prieur.ui.preferences

import androidx.lifecycle.ViewModel
import fr.enssat.pokerplanning.ledeunff_prieur.data.User


class PreferencesViewModel : ViewModel() {
    lateinit var saveCurrentUser: (user: User)->Unit
    lateinit var user: User

    fun savePreferences(){
        saveCurrentUser(user)
    }
}