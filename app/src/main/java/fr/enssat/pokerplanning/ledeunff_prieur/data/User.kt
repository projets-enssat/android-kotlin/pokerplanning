package fr.enssat.pokerplanning.ledeunff_prieur.data

import com.google.gson.Gson
import fr.enssat.pokerplanning.ledeunff_prieur.functions.generateUID

data class User(
    override val id: String = generateUID(),

    var first_name: String = "",
    var last_name: String = ""
):DataClass{
    companion object{
        fun fromJson(json: Any?): User{
            return Gson().fromJson(json.toString(), User::class.java)
        }
    }

    override fun equals(other: Any?): Boolean {
        if(this === other) return true
        if(other?.javaClass != javaClass) return false
        other as User
        return other.id === this.id
    }

    override fun hashCode(): Int {
        return this.id.hashCode()
    }

    override fun toString(): String{
        return Gson().toJson(this)
    }
}
