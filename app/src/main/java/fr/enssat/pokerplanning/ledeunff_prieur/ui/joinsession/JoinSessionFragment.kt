package fr.enssat.pokerplanning.ledeunff_prieur.ui.joinsession

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import fr.enssat.pokerplanning.ledeunff_prieur.JoinSessionActivity
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.adapter.SessionAdapter
import fr.enssat.pokerplanning.ledeunff_prieur.constant.MESSAGE
import fr.enssat.pokerplanning.ledeunff_prieur.databinding.FragmentJoinSessionBinding
import fr.enssat.pokerplanning.ledeunff_prieur.functions.getViewModel

class JoinSessionFragment : Fragment() {
    private lateinit var binding: FragmentJoinSessionBinding
    private lateinit var joinSessionViewModel: JoinSessionViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        joinSessionViewModel = getViewModel()
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_join_session, container, false)

        activity?.let { joinSessionViewModel.runMulticastAgent(it)}

        val adapter =
            SessionAdapter {
                joinSessionViewModel.joinSession(it)
                // listener qui fermera l'activite si l'hote envoie un session close
                activity?.let{
                    joinSessionViewModel.onSessionClosed = this.backToSessionList(it)
                }
                Toast.makeText(this.context, "session clicked", Toast.LENGTH_SHORT).show()
                nextView()
            }

        binding.messageList.adapter = adapter
        joinSessionViewModel.sessions.observe(this, Observer { list ->
            adapter.sessionList = list
        })

        joinSessionViewModel.multicastEmit(MESSAGE.SESSIONS_REQUEST)

        return binding.root
    }

    fun nextView(){
        val navCtrl = NavHostFragment.findNavController(this)
        navCtrl.navigate(R.id.navigation_wait_session_start)
    }

    fun backToSessionList(context: Context?): ()->Unit{
        return {
            val intent = Intent(context, JoinSessionActivity::class.java)
            Toast.makeText(context, "Session has been closed by host", Toast.LENGTH_SHORT).show()
            startActivity(intent)
        }
    }
}