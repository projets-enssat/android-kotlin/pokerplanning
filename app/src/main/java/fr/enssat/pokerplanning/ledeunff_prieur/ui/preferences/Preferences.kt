package fr.enssat.pokerplanning.ledeunff_prieur.ui.preferences

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable


data class Preferences(
    private var _firstName: String,
    private var _lastName: String
): BaseObservable(){

    var firstName: String
    @Bindable get() = _firstName
    set(value){
        _firstName = value
    }

    var lastName: String
    @Bindable get() = _lastName
    set(value){
        _lastName = value
    }
}