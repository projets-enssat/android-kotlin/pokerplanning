package fr.enssat.pokerplanning.ledeunff_prieur.ui.createsession

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.adapter.UserAdapter
import fr.enssat.pokerplanning.ledeunff_prieur.databinding.FragmentWaitUsersBinding
import fr.enssat.pokerplanning.ledeunff_prieur.functions.getViewModel

class WaitUsersFragment : Fragment() {
    private lateinit var binding: FragmentWaitUsersBinding
    private lateinit var sessionViewModel: SessionViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        sessionViewModel = getViewModel()

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wait_users, container, false)
        binding.vm  = sessionViewModel

        val adapter = UserAdapter {}
        binding.usersList.adapter = adapter

        sessionViewModel.users.observe(this, Observer {
            list -> adapter.users = list
        })

        return binding.root
    }

}