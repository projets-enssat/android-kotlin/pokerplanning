package fr.enssat.pokerplanning.ledeunff_prieur.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.data.Feature
import fr.enssat.pokerplanning.ledeunff_prieur.data.User
import fr.enssat.pokerplanning.ledeunff_prieur.data.Vote


class ExpandableViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    private val expandableHeader: Toolbar = view.findViewById(R.id.recycler_header)
    private val expandableContent: RecyclerView = view.findViewById(R.id.recycler_content)

    fun setHeader(feature: Feature) {
        expandableHeader.title = feature.title
    }

    fun setContent(users: List<User>, votes: List<Vote>) {

        val adapter = UserAdapter {}
        expandableContent.adapter = adapter

        adapter.users = users
        adapter.votes = votes
    }
}

class ExpandableAdapter(val listener: (String) -> Unit): RecyclerView.Adapter<ExpandableViewHolder>() {

    var features: List<Feature> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    var users: List<User> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return features.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpandableViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.recyclerview_expandable_item_user, parent, false)
        return ExpandableViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: ExpandableViewHolder, position: Int) {
        holder.setHeader(feature = features[position])
        holder.setContent(users = users, votes = features[position].votes.toList())
    }

}