package fr.enssat.pokerplanning.ledeunff_prieur

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomnavigation.BottomNavigationView
import fr.enssat.pokerplanning.ledeunff_prieur.constant.HOSTFRAGMENT
import fr.enssat.pokerplanning.ledeunff_prieur.functions.setMenuListener
import fr.enssat.pokerplanning.ledeunff_prieur.ui.createsession.*


class CreateSessionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_session)

        // retrieve session view model to provide router
        val sessionViewModel = ViewModelProviders.of(this).get(SessionViewModel::class.java)

        // declare router that contains a methode navigate to switch between fragments
        val router = Router(this)
        router.route(HOSTFRAGMENT.CREATE_SESSION, CreateSessionFragment::class.java)
        router.route(HOSTFRAGMENT.WAIT_USERS, WaitUsersFragment::class.java)
        router.route(HOSTFRAGMENT.CREATE_FEATURE, CreateFeatureFragment::class.java)
        router.route(HOSTFRAGMENT.WAIT_VOTES, WaitVotesFragment::class.java)
        router.route(HOSTFRAGMENT.PUBLISH_VOTES, PublishVotesFragment::class.java)
        router.route(HOSTFRAGMENT.PUBLISH_RESULTS, PublishResultsFragment::class.java)
        sessionViewModel.router = router

        // create and bind bottom nav menu
        val navigation: BottomNavigationView = findViewById(R.id.nav_view)
        setMenuListener(navigation, R.id.navigation_create_session)
    }

}
