package fr.enssat.pokerplanning.ledeunff_prieur.functions

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import fr.enssat.pokerplanning.ledeunff_prieur.CreateSessionActivity
import fr.enssat.pokerplanning.ledeunff_prieur.JoinSessionActivity
import fr.enssat.pokerplanning.ledeunff_prieur.ProfileActivity
import fr.enssat.pokerplanning.ledeunff_prieur.R

fun openActivity(activity: AppCompatActivity, activity_id: Int, check:Boolean=true): Boolean{
    when(activity_id) {
        R.id.navigation_join_session -> {
            val intent = Intent(activity, JoinSessionActivity::class.java)
            activity.startActivity(intent)
            activity.finish()
            return true
        }
        R.id.navigation_create_session -> {
            val intent = Intent(activity, CreateSessionActivity::class.java)
            activity.startActivity(intent)
            activity.finish()
            return true
        }
        R.id.navigation_preferences -> {
            val intent = Intent(activity, ProfileActivity::class.java)
            activity.startActivity(intent)
            activity.finish()
            return true
        }
        else -> {
            return false
        }
    }
}

fun AppCompatActivity.setMenuListener(navigation: BottomNavigationView, activity_id: Int){
    navigation.selectedItemId = activity_id

    navigation.setOnNavigationItemSelectedListener{
        if(it.itemId==activity_id){
            false
        }else{
            openActivity(this, it.itemId)
        }
    }
}