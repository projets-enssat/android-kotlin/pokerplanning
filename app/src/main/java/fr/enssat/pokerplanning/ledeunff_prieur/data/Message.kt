package fr.enssat.pokerplanning.ledeunff_prieur.data

import com.google.gson.Gson
import fr.enssat.pokerplanning.ledeunff_prieur.constant.MESSAGE

data class Message(
    val type: MESSAGE = MESSAGE.NULL,
    val session_id: String? = null,
    val data: Any? = null
){
    companion object{
        fun fromJson(json: Any?): Message{
            return Gson().fromJson(json.toString(), Message::class.java)
        }
    }

    override fun toString(): String{
        return Gson().toJson(this)
    }
}