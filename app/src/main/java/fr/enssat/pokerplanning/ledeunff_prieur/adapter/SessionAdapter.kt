package fr.enssat.pokerplanning.ledeunff_prieur.adapter;

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.data.Session

class SessionViewHolder(val view: View): RecyclerView.ViewHolder(view) {
    private val sessionTitleView = view.findViewById<TextView>(R.id.session_title)
    private val sessionOwnerView = view.findViewById<TextView>(R.id.session_owner)
    private val sessionIPView = view.findViewById<TextView>(R.id.session_ip)

    fun setSession(session: Session, listener: (Session) -> Unit) {
        sessionTitleView.text = session.title
        sessionOwnerView.text =
            "${session.owner.first_name.capitalize()} ${session.owner.last_name.toUpperCase()}"
        sessionIPView.text = session.ip_address

        view.setOnClickListener { view ->
            Log.d("DEBUG", "onItemClick")
            listener(session)
        }
    }
}

class SessionAdapter(val listener: (Session) -> Unit): RecyclerView.Adapter<SessionViewHolder>() {
    var sessionList: List<Session> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return sessionList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SessionViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.recyclerview_item_session, parent, false)
        return SessionViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: SessionViewHolder, position: Int) {
        holder.setSession(sessionList[position], listener)
    }
}