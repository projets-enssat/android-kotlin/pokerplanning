package fr.enssat.pokerplanning.ledeunff_prieur.network

import android.os.Handler
import android.os.Looper
import android.util.Log
import fr.enssat.pokerplanning.ledeunff_prieur.constant.MESSAGE
import fr.enssat.pokerplanning.ledeunff_prieur.constant.MESSAGE_MAX_SIZE
import fr.enssat.pokerplanning.ledeunff_prieur.data.Message
import fr.enssat.pokerplanning.ledeunff_prieur.data.Session
import fr.enssat.pokerplanning.ledeunff_prieur.data.User
import java.io.IOException
import java.net.InetAddress
import java.net.Socket
import java.nio.charset.StandardCharsets
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class ClientSocket(val messageListener: (Message) -> Unit, val connectionListener: (Boolean) -> Unit) {
    private val TAG = this.javaClass.simpleName

    private lateinit var socket: Socket

    private val mainThread = object : Executor {
        val handler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            handler.post(command)
        }
    }

    fun stop() {
        socket.close()
    }

    fun connect(session: Session, currentUser: User) {
        Executors.newSingleThreadExecutor().execute {
            try {
                val address = InetAddress.getByName(session.ip_address)
                val port = 6791
                Log.d(TAG, "try connecting to $address:$port")
                socket = Socket(address, port)
                mainThread.execute {
                    connectionListener(true)
                    sendAndReceive(MESSAGE.JOIN_SESSION_REQUEST, data=currentUser)
                }
            }
            catch(ex:Exception) {
                mainThread.execute { connectionListener(false) }
            }
        }
    }

    fun sendAndReceive(msgType: MESSAGE = MESSAGE.NULL, session_id: String? = null, data: Any? = null) {
        Executors.newSingleThreadExecutor().execute {
            try {
                val msg = Message(msgType, session_id, data=data.toString())
                val data = msg.toString().toByteArray(StandardCharsets.UTF_8)
                socket.getOutputStream().write(data)
                socket.getOutputStream().flush()

                val buffer = ByteArray(MESSAGE_MAX_SIZE)
                val len = socket.getInputStream().read(buffer)
                val json = String(buffer, 0, len, StandardCharsets.UTF_8)
                val message = Message.fromJson(json)

                //affiche le message reçu dans l'ui
                mainThread.execute { messageListener(message) }

            } catch (e: IOException) {
                mainThread.execute { connectionListener(false) }
                stop()
            }
        }
    }
}
