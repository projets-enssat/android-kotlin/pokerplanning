package fr.enssat.pokerplanning.ledeunff_prieur.ui.createsession

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import fr.enssat.pokerplanning.ledeunff_prieur.CommonViewModel
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.Router
import fr.enssat.pokerplanning.ledeunff_prieur.constant.HOSTFRAGMENT
import fr.enssat.pokerplanning.ledeunff_prieur.constant.MESSAGE
import fr.enssat.pokerplanning.ledeunff_prieur.data.*
import fr.enssat.pokerplanning.ledeunff_prieur.functions.NetUtils
import fr.enssat.pokerplanning.ledeunff_prieur.functions.addOrReplace
import fr.enssat.pokerplanning.ledeunff_prieur.network.BroadcastAgent
import fr.enssat.pokerplanning.ledeunff_prieur.network.ServerSocket
import fr.enssat.pokerplanning.ledeunff_prieur.ui.preferences.StoredPreferences


class SessionViewModel : CommonViewModel() {
    lateinit var router: Router

    var session = Session()
    lateinit var feature: Feature

    // callbacks
    var onCreateSessionValidationError: ()->Unit = {}
    var onCreateFeatureValidationError: ()->Unit = {}

    // features
    private val _features = MutableLiveData<List<Feature>>()
    val features: LiveData<List<Feature>> get() = _features

    fun newFeature(){
        feature = Feature(session_id = session.id)
        _votes.postValue(emptyList())
    }

    fun addFeature() {
        session.features.add(feature)
        _features.postValue(session.features.toList()) // displayed when summary
    }

    // votes
    private val _votes = MutableLiveData<List<Vote>>()
    val votes: LiveData<List<Vote>> get() = _votes

    fun addVote(vote: Vote) {
        feature.votes.add(vote)
        _votes.postValue(feature.votes.toList())
    }

    // Users
    val usersSet: MutableSet<User> = mutableSetOf()
    val _users: MutableLiveData<List<User>> = MutableLiveData()
    // observable users
    val users: LiveData<List<User>> = _users

    fun addUser(user: User) {
        usersSet.addOrReplace(user)
        Log.d("wallah", "$usersSet")
        session.users = usersSet.toList()
        _users.postValue(session.users.toList())
    }
    ////

    // network utils
    private val tcpserver = ServerSocket(this::tcpMessageHandler)
    private val multicast = BroadcastAgent(this::multicastMessageHandler)


    fun runSessionServer(context: Context){
        Log.d("poker","starting session servers")

        // listen for multicast messages and tcp connexions
        multicast.startReceiveLoop()
        tcpserver.startListening(NetUtils.getIPAddress(context))

        // configure session ip and owner
        session.owner = StoredPreferences(context).currentUser()
        session.ip_address = NetUtils.getIpString(context)

        // informs client that a new session is available
        multicastEmit(MESSAGE.SESSIONS_REPLY, session_id = session.id, data=session)

        Log.d("poker","session server running...")
    }

    fun stopSessionServer(){
        tcpserver.stopListening()
        multicastEmit(MESSAGE.SESSION_CLOSE, session_id = session.id, stopAfter = true)
    }

    // multicast utils

    fun multicastEmit(msgType: MESSAGE = MESSAGE.NULL, session_id: String? = null, data: Any? = null, stopAfter: Boolean = false){
        val message = Message(msgType, session_id, data=data.toString())
        multicast.send(message.toString(), stopAfter=stopAfter)
    }

    fun multicastMessageHandler(json: String){
        val message = Gson().fromJson(json, Message::class.java)
        onHandleMulticastMessage(message)
    }

    fun onHandleMulticastMessage(message: Message){
        Log.d("poker", "multicast message handled " + message.type)
        Log.d("poker", "message: "+message.toString())
        if(message.type == MESSAGE.SESSIONS_REQUEST){
            // le serveur se signale quand on lui demande qui il est
            multicastEmit(MESSAGE.SESSIONS_REPLY, session_id = session.id, data=session)
        }
    }

    //

    fun tcpMessageHandler(message: Message): Message?{
        Log.d("poker", "TCP message received $message")
        var response: Message? = null

        if(message.type == MESSAGE.JOIN_SESSION_REQUEST){
            Log.d("poker", "${message.data}")
            val user = User.fromJson(message.data)
            addUser(user)
            response = Message(MESSAGE.JOIN_SESSION_REPLY, data=session.toString())
            Log.d("wallah", "current fragment ${session.currentFragment}")
        }else if(message.type == MESSAGE.FEATURE_VOTE_REPLY){
            Log.d("poker", "feature vote received")
            val vote = Vote.fromJson(message.data)
            addVote(vote)
            response = Message(MESSAGE.FEATURE_VOTE_REPLY, data=vote.toString())
        }else if(message.type == MESSAGE.FEATURE_VOTE_UPDATE){
            response = Message(MESSAGE.FEATURE_VOTE_REQUEST, data=feature.toString())
        }

        return response
    }

    // functions called when click on create object
    var onCreateSession: ()->Unit = {}

    fun createSession(context: Context){
        if(session.title === ""){
            onCreateSessionValidationError()
        }else{
            runSessionServer(context)
            router.navigate(HOSTFRAGMENT.WAIT_USERS)
            newFeature()
        }
    }

    fun createSessionSubmit(){
        onCreateSession()
    }

    // open fragment
    fun openCreateFeature(){
        session.currentFragment = HOSTFRAGMENT.CREATE_FEATURE
        newFeature() // clear the feature and votes when a new feature is started
        router.navigate(HOSTFRAGMENT.CREATE_FEATURE)
    }

    fun createFeatureSubmit(){
        if(feature.title===""){
            onCreateFeatureValidationError()
        }else{
            session.currentFragment = HOSTFRAGMENT.WAIT_VOTES
            multicastEmit(MESSAGE.FEATURE_VOTE_REQUEST, session_id = session.id, data=feature)
            router.navigate(HOSTFRAGMENT.WAIT_VOTES)
        }
    }

    fun openPublishVotes(){
        session.currentFragment = HOSTFRAGMENT.PUBLISH_VOTES
        addFeature() // add the current feature to the session to share it to users
        multicastEmit(MESSAGE.PUBLISH_VOTE_RESULTS, session_id = session.id, data=session)
        router.navigate(HOSTFRAGMENT.PUBLISH_VOTES)
    }

    fun openPublishResults(){
        session.currentFragment = HOSTFRAGMENT.PUBLISH_RESULTS
        multicastEmit(MESSAGE.PUBLISH_SESSION_RESULTS, session_id = session.id, data=session)
        router.navigate(HOSTFRAGMENT.PUBLISH_RESULTS)
    }

    override fun leaveSession(){
        session.currentFragment = HOSTFRAGMENT.CREATE_SESSION
        router.open(R.id.navigation_create_session)
    }

    override fun onCleared() {
        super.onCleared()
        stopSessionServer()
        Log.d("poker", "session viewmodel cleared")
    }
}