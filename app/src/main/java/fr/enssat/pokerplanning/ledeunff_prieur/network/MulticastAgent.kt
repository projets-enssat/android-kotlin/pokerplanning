package fr.enssat.pokerplanning.ledeunff_prieur.network

import android.content.Context
import android.net.wifi.WifiManager
import android.os.Handler
import android.os.Looper
import android.util.Log
import fr.enssat.pokerplanning.ledeunff_prieur.constant.MESSAGE_MAX_SIZE
import java.net.*
import java.nio.charset.Charset
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class MulticastAgent(val listener: (String) -> Unit) {
    companion object {
        val TAG = "multicastagent"
        val PORT = 12345
        val MULTICAST_GROUP = InetAddress.getByName("228.1.2.4")
        var multicastLock: WifiManager.MulticastLock? = null

        fun wifiLock(context: Context) {
            Log.d(TAG, "locking wifi")
            var lock = multicastLock
            if (lock == null) {
                val wifi = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
                lock = wifi.createMulticastLock("multicastLock")
                lock.setReferenceCounted(true)
                lock.acquire()
                multicastLock = lock
                Log.d(TAG, "wifi locked")
            }
        }

        fun releaseWifiLock() {
            multicastLock?.release()
            multicastLock = null
        }
    }

    private val socket = createSocket()
    private val mainThread = object : Executor {
        val handler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            handler.post(command)
        }
    }
    private var loop = true

    private fun createSocket(): MulticastSocket {
        val socket = MulticastSocket(PORT)
        socket.joinGroup(MULTICAST_GROUP)
        Log.d(TAG, "create multicast socket:" + MULTICAST_GROUP + "/" + PORT)
        return socket
    }

    fun startReceiveLoop() {
        Executors.newSingleThreadExecutor().execute {
            while (loop) {
                try {
                    val data = receive()
                    Log.d(TAG, "receiving on multicast: $data")
                    mainThread.execute { listener(data) }
                } catch (e: SocketException) {
                    Log.d("poker", "socket closed unexpectedly")
                }
            }
        }
    }

    private fun receive(): String {
        val packet = DatagramPacket(ByteArray(MESSAGE_MAX_SIZE), MESSAGE_MAX_SIZE)
        Log.d(TAG, "waiting for multicast datagram")
        socket.receive(packet)
        return String(packet.data, 0, packet.length, Charset.forName("UTF-8"))
    }

    fun stopReceiveLoop() {
        loop = false
        _closeSocket()
    }

    private fun _closeSocket() {
        Log.d(TAG, "closing multicast socket")
        socket.leaveGroup(MULTICAST_GROUP)
        socket.close()
    }

    fun send(msg: String, stopAfter: Boolean = false) {
        Executors.newSingleThreadExecutor().execute {
            val data = msg.toByteArray(Charset.forName("UTF-8"))
            Log.d(TAG, "publishing on multicast: ${String(data)}")
            val packet = DatagramPacket(data, 0, data.size, MULTICAST_GROUP, PORT)
            socket.send(packet)
            if (stopAfter) {
                stopReceiveLoop()
            }
        }
    }
}
