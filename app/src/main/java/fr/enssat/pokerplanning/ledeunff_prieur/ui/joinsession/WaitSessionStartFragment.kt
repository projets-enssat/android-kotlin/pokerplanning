package fr.enssat.pokerplanning.ledeunff_prieur.ui.joinsession

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.databinding.FragmentWaitSessionStartBinding
import fr.enssat.pokerplanning.ledeunff_prieur.functions.getViewModel

class WaitSessionStartFragment : Fragment() {
    private lateinit var binding: FragmentWaitSessionStartBinding
    private lateinit var joinSessionViewModel: JoinSessionViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        joinSessionViewModel = getViewModel()

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wait_session_start, container, false)
        return binding.root
    }

}