package fr.enssat.pokerplanning.ledeunff_prieur

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import fr.enssat.pokerplanning.ledeunff_prieur.functions.setMenuListener


class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        val navigation: BottomNavigationView = findViewById(R.id.nav_view)
        setMenuListener(navigation, R.id.navigation_preferences)
    }

}
