package fr.enssat.pokerplanning.ledeunff_prieur

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner

abstract class CommonViewModel: ViewModel(){

    abstract fun leaveSession()
}
