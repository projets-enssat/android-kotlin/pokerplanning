package fr.enssat.pokerplanning.ledeunff_prieur.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.pokerplanning.ledeunff_prieur.R

class CardViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    private val text1: TextView = view.findViewById(R.id.card_Text1)
    private val text2: TextView = view.findViewById(R.id.card_Text2)
    private val layout: ConstraintLayout = view.findViewById(R.id.layoutCard)

    fun setNumber(number: Int, listener: (Int) -> Unit) {
        text1.text = number.toString()
        text2.text = number.toString()

        // listeners
        layout.setOnClickListener {
            listener(number)
        }
    }
}

class CardViewAdapter (var listener: (Int) -> Unit): RecyclerView.Adapter<CardViewHolder>() {

    var numbers: List<Int> = emptyList()
        set(l) {
            field = l
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int {
        return numbers.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.recyclerview_card_to_vote, parent, false)
        return CardViewHolder( view )
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        //Log.d("\t ---- [CARD] \t\t ", numbers[position].toString())
        holder.setNumber(number = numbers[position], listener = listener)
    }

}