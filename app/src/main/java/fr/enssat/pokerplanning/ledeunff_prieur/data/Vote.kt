package fr.enssat.pokerplanning.ledeunff_prieur.data

import com.google.gson.Gson
import fr.enssat.pokerplanning.ledeunff_prieur.functions.generateUID


data class Vote(
    var id: String = generateUID(),
    var user_id: String = "",
    var session_id: String = "",

    var value: Int = 0
){
    companion object{
        fun fromJson(json: Any?): Vote{
            return Gson().fromJson(json.toString(), Vote::class.java)
        }
    }

    override fun toString(): String{
        return Gson().toJson(this)
    }
}
