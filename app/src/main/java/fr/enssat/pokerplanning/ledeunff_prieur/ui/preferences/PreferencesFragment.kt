package fr.enssat.pokerplanning.ledeunff_prieur.ui.preferences

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.data.User
import fr.enssat.pokerplanning.ledeunff_prieur.databinding.FragmentPreferencesBinding

class PreferencesFragment : Fragment() {
    private lateinit var prefsmanager: StoredPreferences
    private lateinit var binding: FragmentPreferencesBinding
    private lateinit var preferencesViewModel: PreferencesViewModel

    init{
        instance = this
    }

    companion object{
        var instance: PreferencesFragment? = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        prefsmanager = StoredPreferences(this.context)

        preferencesViewModel = ViewModelProviders.of(this).get(PreferencesViewModel::class.java)
        preferencesViewModel.user = prefsmanager.currentUser()

        preferencesViewModel.saveCurrentUser = { user: User ->
            prefsmanager.saveUser(user)
            //
            Toast.makeText(
                this.context,
                "Profile saved!",
                Toast.LENGTH_SHORT
            ).show()
        }

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_preferences, container, false)
        binding.vm  = preferencesViewModel
        return binding.getRoot()
    }
}