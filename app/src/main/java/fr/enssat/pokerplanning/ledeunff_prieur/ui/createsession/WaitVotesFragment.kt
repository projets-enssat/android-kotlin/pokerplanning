package fr.enssat.pokerplanning.ledeunff_prieur.ui.createsession

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.databinding.FragmentWaitVotesBinding
import fr.enssat.pokerplanning.ledeunff_prieur.functions.getViewModel

class WaitVotesFragment : Fragment() {
    private lateinit var binding: FragmentWaitVotesBinding
    private lateinit var sessionViewModel: SessionViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        sessionViewModel = getViewModel()

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wait_votes, container, false)
        binding.vm  = sessionViewModel

        sessionViewModel.users.observe(this, Observer {
            updateCounter()
        })
        sessionViewModel.votes.observe(this, Observer{
            updateCounter()
        })

        return binding.root
    }

    fun updateCounter(){
        val nb_votes = sessionViewModel.votes.value?.size?.toString() ?: "0"
        val nb_users = sessionViewModel.users.value?.size?.toString() ?: "0"
        binding.countNumberVotes.text = "$nb_votes/$nb_users"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateCounter()
    }

}