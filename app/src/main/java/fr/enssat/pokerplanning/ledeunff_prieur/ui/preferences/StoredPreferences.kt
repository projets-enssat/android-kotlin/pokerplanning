package fr.enssat.pokerplanning.ledeunff_prieur.ui.preferences

import android.content.Context
import fr.enssat.pokerplanning.ledeunff_prieur.data.User
import fr.enssat.pokerplanning.ledeunff_prieur.functions.generateUID
import fr.enssat.pokerplanning.ledeunff_prieur.functions.randomString


class StoredPreferences(context: Context?){
    var prefs = context!!.getSharedPreferences("ProfilePrefs", 0)

    //
    val USER_ID_KEY = "user_id"
    val FIRST_NAME_KEY = "first_name"
    val LAST_NAME_KEY = "last_name"

    //
    fun getID(): String{
        val user_id = this.prefs.getString(USER_ID_KEY, generateUID()).toString()
        setID(user_id)
        return user_id
    }

    fun setID(value: String){
        this.prefs.edit().putString(USER_ID_KEY, value).apply()
    }

    //
    fun getFirstName():String{
        return this.prefs.getString(FIRST_NAME_KEY, "Guest").toString()
    }

    fun setFirstName(value: String){
        this.prefs.edit().putString(FIRST_NAME_KEY, value).apply()
    }

    //
    fun getLastName():String{
        return this.prefs.getString(LAST_NAME_KEY, randomString()).toString()
    }

    fun setLastName(value: String){
        this.prefs.edit().putString(LAST_NAME_KEY, value).apply()
    }

    fun saveUser(user: User){
        setFirstName(user.first_name)
        setLastName(user.last_name)
    }

    fun currentUser(): User{
        return User(id=getID(), first_name = getFirstName(), last_name = getLastName())
    }
}