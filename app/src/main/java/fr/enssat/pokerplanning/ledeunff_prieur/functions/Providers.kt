package fr.enssat.pokerplanning.ledeunff_prieur.functions

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.data.DataClass


fun Fragment.getViewModelProvider() =
    activity?.let(ViewModelProviders::of) ?: ViewModelProviders.of(this)


inline fun <reified T : ViewModel> Fragment.getViewModel() =
    getViewModelProvider().get(T::class.java)


fun AppCompatActivity.routeFor(fragmentId: Int, fragmentClass: Class<out Fragment>): ()->Unit{
    return {
        val fragment = fragmentClass.newInstance()
        val navHost = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)

        navHost!!.childFragmentManager.let { manager ->
            val transaction = manager.beginTransaction()
            transaction.add(fragmentId, fragment)
            for (fragment in manager.fragments) {
                transaction.remove(fragment)
            }
            transaction.commit()
        }
    }
}


inline fun <reified T : DataClass> MutableSet<T>.addOrReplace(instance: T){
    this.removeIf{
        it.id == instance.id
    }
    this.add(instance)
}