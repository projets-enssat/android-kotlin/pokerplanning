package fr.enssat.pokerplanning.ledeunff_prieur.ui.joinsession

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.adapter.UserAdapter
import fr.enssat.pokerplanning.ledeunff_prieur.databinding.FragmentWaitOtherParticipantsBinding
import fr.enssat.pokerplanning.ledeunff_prieur.functions.getViewModel

class WaitOtherParticipantsFragment : Fragment() {
    private lateinit var binding: FragmentWaitOtherParticipantsBinding
    private lateinit var joinSessionViewModel: JoinSessionViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        joinSessionViewModel = getViewModel()

        // TODO: wait for signal before display next view

        //nextView()

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wait_other_participants, container, false)

        val adapter = UserAdapter {}
        binding.usersList.adapter = adapter

        joinSessionViewModel.allUsers.observe(this, Observer {
            list->adapter.users = list
        })

        joinSessionViewModel.currentFeature.observe(this, Observer {
                feature->adapter.votes = feature.votes
        })

        return binding.root
    }
}