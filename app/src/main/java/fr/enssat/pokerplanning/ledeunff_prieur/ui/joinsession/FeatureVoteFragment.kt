package fr.enssat.pokerplanning.ledeunff_prieur.ui.joinsession

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import fr.enssat.pokerplanning.ledeunff_prieur.R
import fr.enssat.pokerplanning.ledeunff_prieur.adapter.CardViewAdapter
import fr.enssat.pokerplanning.ledeunff_prieur.databinding.FragmentFeatureVoteBinding
import fr.enssat.pokerplanning.ledeunff_prieur.functions.getViewModel

class FeatureVoteFragment : Fragment() {
    private lateinit var binding: FragmentFeatureVoteBinding
    private lateinit var joinSessionViewModel: JoinSessionViewModel
    private var NUMBERS = listOf(1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        joinSessionViewModel = getViewModel()

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_feature_vote, container, false)

        // if current vote's feature is updated, changes are reflected in xml
        joinSessionViewModel.currentFeature.observe(this, Observer {
            Toast.makeText(this.context, "feature changed with title ${it.title}", Toast.LENGTH_SHORT).show()
            binding.textFeatureTitle.text = it.title
            binding.textFeatureDescription.text = it.description
        })

        // create grid of buttons with fibonacci values
        var adapter = CardViewAdapter(joinSessionViewModel.onVoteFeature)
        binding.CardList.adapter = adapter
        binding.CardList.layoutManager = GridLayoutManager(this.context, 4)

        adapter.numbers = NUMBERS

        return binding.root
    }
}