package fr.enssat.pokerplanning.ledeunff_prieur.functions

import java.lang.StringBuilder
import java.util.UUID
import kotlin.random.Random

fun generateUID(): String{
    return UUID.randomUUID().toString()
}

fun randomString(): String{
    val LENGTH = 10
    val ALPHA = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray()
    var sb = StringBuilder(LENGTH)
    var char: Char
    for(i in 1..LENGTH){
        char = ALPHA[Random.nextInt(ALPHA.size)]
        sb.append(char)
    }
    return sb.toString()
}
