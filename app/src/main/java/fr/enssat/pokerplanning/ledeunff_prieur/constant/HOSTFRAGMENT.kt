package fr.enssat.pokerplanning.ledeunff_prieur.constant

enum class HOSTFRAGMENT: FRAGMENT{
    CREATE_SESSION,
    WAIT_USERS,
    CREATE_FEATURE,
    WAIT_VOTES,
    PUBLISH_VOTES,
    PUBLISH_RESULTS
}