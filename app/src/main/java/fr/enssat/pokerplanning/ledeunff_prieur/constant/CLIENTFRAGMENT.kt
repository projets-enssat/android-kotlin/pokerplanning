package fr.enssat.pokerplanning.ledeunff_prieur.constant

enum class CLIENTFRAGMENT: FRAGMENT {
    LIST_SESSIONS,
    WAITING_SESSION_START,
    FEATURE_VOTE,
    WAITING_VOTES,
    SESSION_RESULTS
}