package fr.enssat.pokerplanning.ledeunff_prieur.functions

import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.os.Build
import android.text.format.Formatter
import java.net.Inet4Address
import java.net.InetAddress

class NetUtils {
    companion object{
        fun getIpString(context: Context): String {
            var result : String

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val linkInfos = cm.getLinkProperties(cm.activeNetwork)?.linkAddresses
                val address = linkInfos?.filter { it -> it.address is Inet4Address }?.single()
                result = address!!.address.hostAddress
            }
            else {

                val wm = context.getSystemService(WIFI_SERVICE) as WifiManager
                result = Formatter.formatIpAddress(wm.connectionInfo.ipAddress)
            }

            return result
        }

        fun getIPAddress(context: Context): InetAddress{
            return InetAddress.getByName(getIpString(context))
        }
    }
}