package fr.enssat.pokerplanning.ledeunff_prieur

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import fr.enssat.pokerplanning.ledeunff_prieur.constant.FRAGMENT
import fr.enssat.pokerplanning.ledeunff_prieur.functions.openActivity


class Router(val activity: AppCompatActivity){
    val fragments: HashMap<FRAGMENT, Class<out Fragment>> = HashMap()

    fun route(tag: FRAGMENT, fragmentClass: Class<out Fragment>){
        fragments.put(tag, fragmentClass)
    }

    fun open(activity_id: Int){
        openActivity(activity, activity_id, check=false)
    }

    fun navigate(tag: FRAGMENT){
        val fragmentClass = fragments.get(tag)
        if(fragmentClass != null){
            val fragment = fragmentClass.newInstance()
            val navHost = activity.supportFragmentManager.findFragmentById(R.id.nav_host_fragment)

            navHost!!.childFragmentManager.let { manager ->
                val transaction = manager.beginTransaction()
                transaction.add(R.id.nav_host_fragment, fragment)
                for (fragment in manager.fragments) {
                    transaction.remove(fragment)
                }
                transaction.commit()
            }
        }
    }
}